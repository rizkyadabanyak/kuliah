import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // home: const MyHomePage(title: 'Flutter Demo Home Page'),
      home: const DetailScreen(),
    );
  }
}

class DetailScreen extends StatelessWidget {
  const DetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset('assets/mokasel.jpg'),
          Container(
            margin: EdgeInsets.only(top: 16.0),
            child: Text(
              'Aku anak sehat tubuhku kuat qwdqwd qwdqw',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.bold
              ),)
          ),

          Container(
            margin: EdgeInsets.only(top: 16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    Icon(Icons.date_range),
                    Text('Open Everiday')
                  ],
                ),Column(
                  children: [
                    Icon(Icons.punch_clock),
                    Text('Open Everiday')
                  ],
                ),Column(
                  children: [
                    Icon(Icons.date_range),
                    Text('Open Everiday')
                  ],
                ),
              ],
            )
          ),Container(
            margin: EdgeInsets.only(top: 16.0),
            child: Text(
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vehicula malesuada egestas. Integer vehicula neque eget viverra facilisis. Nulla a dignissim libero. Etiam bibendum dignissim est eget bibendum. Nulla facilisi. Nam finibus laoreet est nec lacinia. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla viverra urna turpis, nec blandit quam dapibus non. Pellentesque sit amet turpis sit amet lectus eleifend suscipit eget sodales nulla',
              textAlign: TextAlign.center,

            )
          ),
        ],
      ),
    );
  }
}
