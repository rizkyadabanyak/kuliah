// // ignore_for_file: public_member_api_docs, sort_constructors_first
// import 'dart:convert';
//
// class TourismPlace {
//   String name;
//   String location;
//   String imageAsset;
//   String desk;
//   String detailImg1;
//   String? detailImg2;
//   String? detailImg3;
//
//   TourismPlace({
//     required this.name,
//     required this.location,
//     required this.imageAsset,
//     required this.desk,
//     required this.detailImg1,
//     this.detailImg2,
//     this.detailImg3,
//   });
// }
// final List<TourismPlace> doneTourismPlaceList = [];
// final List<TourismPlace> tourismPlaceList = [
//   TourismPlace(
//     name: 'Surabaya Submarine Monument',
//     location: 'Jl Pemuda',
//     imageAsset: 'assets/mokasel.jpg',
//     desk: 'Surabaya Submarine Monument or as known as Monumen Kapal Selam (Monkasel) is the largest submarine monument in Asia, which was built in riverside of Kalimas, Surabaya. This monument was built by idea of Navy Veterans.',
//     detailImg1: 'assets/kasel.jpeg',
//     detailImg2: 'assets/kasel2.jpeg',
//     detailImg3: 'assets/kasel3.jpeg',
//   ),
//   TourismPlace(
//     name: 'Tuguh pahlawan',
//     location: 'Kenjeran',
//     imageAsset: 'assets/tuguhpahlawan.jpg',
//
//     desk:'Kelenteng Sanggar Agung atau Klenteng Hong San Tang adalah sebuah klenteng di Kota Surabaya. Alamatnya berada di Jalan Sukolilo Nomor 100, Pantai Ria Kenjeran, Surabaya. Kuil ini, selain menjadi tempat ibadah bagi pemeluk Tridharma, juga menjadi tempat tujuan wisata bagi para wisatawan. Klenteng ini dibuka pada tahun 1999.',
//     detailImg1: 'assets/kasel.jpeg',
//     detailImg2: 'assets/kasel2.jpeg',
//     detailImg3: 'assets/kasel3.jpeg',
//   ),
//   TourismPlace(
//     name: 'karimun jawa',
//     location: 'Krembangan Utara',
//     imageAsset: 'assets/karimun_jawa.jpg',
//     desk:
//         'Museum rokok ini dulunya adalah pabrik rokok pertama Sampoerna. Saat ini bangunan bergaya kolonial Belanda yang dibangun sekitar tahun 1862 termasuk dalam situs bersejarah yang dilestarikan di Surabaya. Pada masa Belanda, bangunan ini adalah panti asuhan yang dikelola Belanda. Kemudian pada tahun 1932 dibeli oleh Liem Seeng Tee yang menjadi pendiri Sampoerna dan menjadi tempat pertama produksi rokok Sampoerna. Saat ini, bangunan ini termasuk dalam situs sejarah yang dilestarikan di Surabaya.',
//     detailImg1: 'assets/kasel.jpeg',
//
//   ),
//   TourismPlace(
//     name: 'Pacet',
//     location: 'Alun-alun contong',
//     imageAsset: 'assets/pacet.jpg',
//     desk: 'Tugu Pahlawan adalah salah satu peninggalan bersejarah di Surabaya. Mengutip dari situs Badan Perencanaan Pembangunan Kota Surabaya, Tugu Pahlawan berada di Jl. Pahlawan, Alun-alun Contong, Kec. Bubutan, Kota Surabaya, Jawa Timur.',
//     detailImg1: 'assets/kasel.jpeg',
//   ),
//   TourismPlace(
//     name: 'Paris',
//     location: 'Wonokromo',
//     imageAsset: 'assets/paris.jpg',
//     desk: '',
//     detailImg1: 'assets/kasel.jpeg',
//
//   ),
// ];

class TourismPlace {
  String name;
  String location;
  String imageAssets;
  String description;
  String day;
  String callender;
  String price;

  TourismPlace({
    required this.name,
    required this.location,
    required this.imageAssets,
    required this.description,
    required this.day,
    required this.callender,
    required this.price,
  });
}
