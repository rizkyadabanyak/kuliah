import 'package:flutter/material.dart';
// import 'package:praktikum_mobile/models/tourism_place.dart';
// import 'package:praktikum_mobile/provider/done_tourism_provider.dart';
import 'package:provider/provider.dart';

import '../model/tourism_place.dart';
import '../provider/done_tourism_provider.dart';

class DoneTourismPlaceList extends StatelessWidget {
  const DoneTourismPlaceList({super.key});

  @override
  Widget build(BuildContext context) {
    final List<TourismPlace> doneTourismPlaceList =
        Provider.of<DoneTourismProvider>(
      context,
      listen: false,
    ).doneTourismPlaceList;
    for (var element in doneTourismPlaceList) {
      print('done tourism place = ${element.callender}');
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text("Wisata Telah Dikunjungi"),
      ),
      body: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          final TourismPlace place = doneTourismPlaceList[index];
          return Card(
            color: Colors.white60,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Image.asset(place.imageAssets),
                ),
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          place.name,
                          style: const TextStyle(fontSize: 16.0),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(place.location)
                      ],
                    ),
                  ),
                ),
                const Icon(Icons.download_done_sharp, color: Colors.green)
              ],
            ),
          );
        },
        itemCount: doneTourismPlaceList.length,
      ),
    );
  }
}
