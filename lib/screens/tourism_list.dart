// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
// import 'package:praktikum_mobile/provider/done_tourism_provider.dart';
// import 'package:praktikum_mobile/screens/detail_screen.dart';
// import 'package:praktikum_mobile/screens/done_tourism_list.dart';
// import 'package:praktikum_mobile/screens/list_item.dart';
import 'package:provider/provider.dart';

import '../model/tourism_place.dart';
import '../provider/done_tourism_provider.dart';
import 'detail_screen.dart';
import 'list_item.dart';

// import '../models/tourism_place.dart';

class TourismList extends StatefulWidget {
  const TourismList({Key? key}) : super(key: key);

  @override
  _TourismListState createState() => _TourismListState();
}

class _TourismListState extends State<TourismList> {
  List<TourismPlace> doneTourismPlaceList = [];
  final List<TourismPlace> tourismPlaceList = [
    TourismPlace(
      name: "Tugu Ale-Ale",
      location: "Ketapang, Kalbar",
      imageAssets: "assets/images/tugu-ale-ale-1.jpg",
      description:
          "1 Tugu Ale-Ale Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      day: "01:00-02:00",
      callender: "Senin-Selasa",
      price: "10.000",
    ),
    TourismPlace(
      name: "Wisata Pantai Pulau Datok - Sukadana",
      location: "Sukadana, Ketapang, Kalbar",
      imageAssets: "assets/images/pulau-datok-3.jpg",
      description:
          "Wisata Pantai Pulau Datok - Sukadana 2 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      day: "03:00-04:00",
      callender: "Selase-Rabu",
      price: "20.000",
    ),
    TourismPlace(
      name: "name3",
      location: "location3",
      imageAssets: "assets/images/tugu-ale-ale-3.jpg",
      description:
          "3Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      day: "04:00-05:00",
      callender: "Kamis-Jum'at",
      price: "40.000",
    ),
    TourismPlace(
      name: "name4",
      location: "location4",
      imageAssets: "assets/images/tugu-ale-ale-3.jpg",
      description:
          "4Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      day: "04:00-05:00",
      callender: "Kamis-Jum'at",
      price: "40.000",
    ),
    TourismPlace(
      name: "name5",
      location: "location5",
      imageAssets: "assets/images/tugu-ale-ale-1.jpg",
      description:
          "5Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      day: "04:00-05:00",
      callender: "Kamis-Jum'at",
      price: "40.000",
    ),
    TourismPlace(
      name: "name6",
      location: "location6",
      imageAssets: "assets/images/tugu-ale-ale-2.jpg",
      description:
          "6Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      day: "04:00-05:00",
      callender: "Kamis-Jum'at",
      price: "40.000",
    ),
    TourismPlace(
      name: "name7",
      location: "location7",
      imageAssets: "assets/images/tugu-ale-ale-1.jpg",
      description:
          "7Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      day: "04:00-05:00",
      callender: "Kamis-Jum'at",
      price: "40.000",
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        final TourismPlace place = tourismPlaceList[index];
        return InkWell(
          onTap: () {
            Navigator.push(context, MaterialPageRoute(
              builder: (context) {
                return DetailScreen(place: place);
              },
            ));
          },
          child: Consumer<DoneTourismProvider>(
            builder: (context, DoneTourismProvider data, widget) {
              return ListItem(
                place: place,
                isDone: doneTourismPlaceList.contains(place),
                onCheckboxClick: (bool? value) {
                  setState(() {
                    if (value != null) {
                      value
                          ? doneTourismPlaceList.add(place)
                          : doneTourismPlaceList.remove(place);
                      data.complete(place, value);
                    }
                  });
                },
              );
            },
          ),
        );
      },
      itemCount: tourismPlaceList.length,
    );
  }

}
